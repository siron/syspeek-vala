using Gtk;

#if DLMALLOC
  extern void set_vtable ();
  extern int  dlmalloc_trim (size_t pad);
  extern void print_malloc_table ();
  extern void print_malloc_info  ();
#endif

class SysPeek {
  private const string CPU_STATUS       = "/proc/stat";
  private const string MEMORY_STATUS    = "/proc/meminfo";
  private const string DISK_STATUS      = "/home";
  private const string INTERFACE_STATUS = "/proc/net/route";
  private const string NETWORK_STATUS   = "/proc/net/dev";

  public  SystemIndicator appIcon { get; set; }

//  -------------------------------------------------------------------------------------
//  Split a line of data into tokens
//  -------------------------------------------------------------------------------------

  private string [] tokenize (string line, int size) {
    string [] token = new string [size];

    var tokens = line.split_set (" \t", 0);
    int i = 0;  foreach (string x in tokens) {
      if (x.length > 0) token [i++] = x;
      if (i == size) break;
    }

    token.resize (i == 0 ? 1 : i);
    return token;
  }

//  -------------------------------------------------------------------------------------
//  CPU usage
//  -------------------------------------------------------------------------------------

  private int64 cpuTotal = 0;
  private int64 cpuBusy  = 0;

  private void updateCpu () {
    try {
      var file  = File.new_for_path (CPU_STATUS);
      var data  = new DataInputStream (file.read ());
      var line  = data.read_line (null);
      var token = tokenize (line, 10);

      int64 total = 0;
      for (int i = 0; i < token.length-1; i++) total += int64.parse (token [i]);

      var idle       = int64.parse (token [4]);
      var busy       = total - idle;
      var deltaBusy  = (float) busy  - cpuBusy;
      var deltaTotal = (float) total - cpuTotal;
      var percentage = (deltaTotal == 0.0f) ? 0.0f : (deltaBusy / deltaTotal);

      cpuTotal = total;
      cpuBusy  = busy;

      appIcon.update (0, percentage, 100.0f * percentage);

    } catch (Error e) {
      error ("updateCPU: %s", e.message);
    }
  }

//  -------------------------------------------------------------------------------------
//  Memory usage
//  -------------------------------------------------------------------------------------

  private void updateMemory () {
    try {
      int64 memUsed = 0, swapUsed = 0, memTotal = 0, swapTotal = 0;
      var file = File.new_for_path (MEMORY_STATUS);
      var data = new DataInputStream (file.read ());

      string line;
      while ((line = data.read_line (null)) != null) {
        var token = tokenize (line, 2);
        switch (token [0]) {
          case "MemTotal:"  :  memUsed  += (memTotal = int64.parse (token [1]) * 1024);   break;
          case "MemFree:"   :  memUsed  -= int64.parse (token [1]) * 1024;                break;
          case "Buffers:"   :  memUsed  -= int64.parse (token [1]) * 1024;                break;
          case "Cached:"    :  memUsed  -= int64.parse (token [1]) * 1024;                break;
          case "SwapTotal:" :  swapUsed += (swapTotal = int64.parse (token [1]) * 1024);  break;
          case "SwapFree:"  :  swapUsed -= int64.parse (token [1]) * 1024;                break;
        }
      }

      var memPct  = (memTotal  == 0) ? 0.0f : ((float) memUsed  / (float) memTotal);
      var swapPct = (swapTotal == 0) ? 0.0f : ((float) swapUsed / (float) swapTotal);

      appIcon.update (2, memPct,  (float) memUsed,  (float) memTotal);
      appIcon.update (3, swapPct, (float) swapUsed, (float) swapTotal);

    } catch (Error e) {
      error ("updateMemory: %s", e.message);
    }
  }

//  -------------------------------------------------------------------------------------
//  Disk usage
//  -------------------------------------------------------------------------------------

  private void updateDisk () {
    try {
      var dir   = File.new_for_path (DISK_STATUS);
      var size  = dir.query_filesystem_info (FileAttribute.FILESYSTEM_SIZE);
      var used  = dir.query_filesystem_info (FileAttribute.FILESYSTEM_USED);

      var iSize = int64.parse (size.get_attribute_as_string (FileAttribute.FILESYSTEM_SIZE));
      var iUsed = int64.parse (used.get_attribute_as_string (FileAttribute.FILESYSTEM_USED));

      var percentage = (iSize == 0) ? 0.0f : ((float) iUsed / (float) iSize);
      appIcon.update (5, percentage, (float) iUsed,  (float) iSize);

    } catch (Error e) {
      error ("updateDisk: %s", e.message);
    }
  }

//  -------------------------------------------------------------------------------------
//  Network usage
//  -------------------------------------------------------------------------------------

  int64 rxSave =  0;
  int64 txSave =  0;
  int64 rxMax  = -1;
  int64 txMax  = -1;

  private string interfaceName () {
    try {
      var file = File.new_for_path (INTERFACE_STATUS);
      var data = new DataInputStream (file.read ());

      string line;
      while ((line = data.read_line (null)) != null) {
        var token = tokenize (line, 4);
        if (token [3] == "0003") return token [0];
      }
      return (string) null;
    } catch (Error e) {
      error ("interfaceName: %s", e.message);
    }
  }

  private void updateNetwork () {
    try {
      int64 rxTotal = 0, txTotal = 0;
      var ifc = interfaceName ();

      if (ifc != (string) null) {
        var file = File.new_for_path (NETWORK_STATUS);
        var data = new DataInputStream (file.read ());

        string line;
        while ((line = data.read_line (null)) != null) {
          var token = tokenize (line, 10);
          if (token.length == 10 && token [0] == (ifc + ":")) {
            rxTotal = int64.parse (token [1]);
            txTotal = int64.parse (token [9]);
            break;
          }
        }
      }

      int64 rxDelta = rxTotal - rxSave;
      int64 txDelta = txTotal - txSave;

      rxSave = rxTotal;
      txSave = txTotal;

      rxMax  = (rxMax < 0) ? 0 : (rxDelta > rxMax) ? rxDelta : (rxDelta + 9 * rxMax) / 10;
      txMax  = (txMax < 0) ? 0 : (txDelta > txMax) ? txDelta : (txDelta + 9 * txMax) / 10;

      var rxPct = (rxMax == 0) ? 0.0f : ((float) rxDelta / (float) rxMax);
      var txPct = (txMax == 0) ? 0.0f : ((float) txDelta / (float) txMax);

      appIcon.update (7, rxPct, (float) rxDelta, (float) rxTotal);
      appIcon.update (8, txPct, (float) txDelta, (float) txTotal);

    } catch (Error e) {
      error ("updateNetwork: %s", e.message);
    }
  }

  #if THREADED
    public void* run () {
      while (true) {
        updateCpu     ();
        updateMemory  ();
        updateDisk    ();
        updateNetwork ();
        Thread.usleep (1000000);
      }
    }
  #else
    public bool run () {
//    dlmalloc_trim (0);
      updateCpu     ();
      updateMemory  ();
      updateDisk    ();
      updateNetwork ();
      
      #if DLMALLOC
        if ((loops % 60) == 0) {
          stderr.printf ("%4d: ", loops / 60);
          print_malloc_info ();
        }
      #endif
      
      if (maxLoops > 0 && loops > maxLoops) main_quit ();
      loops++;
      appIcon.checkVisibility ();
      return true;
    }
  #endif

  private int loops = 0;
  public  int maxLoops = 0;

//  -------------------------------------------------------------------------------------

  static int main (string [] args) {
    #if DLMALLOC
      set_vtable ();
    #endif

    #if PROFILE
      GLib.Environment.atexit (GLib.mem_profile);
    #endif

    #if DLMALLOC
      GLib.Environment.atexit (print_malloc_table);
    #endif

    Gtk.init (ref args);

    var syspeek     = new SysPeek ();
    syspeek.appIcon = new SystemIndicator ();

    if (args.length > 1) syspeek.maxLoops = int.parse (args [1]);

    #if THREADED
      if (!Thread.supported ()) {
        stderr.printf ("Cannot run without thread support.\n");
        return 1;
      }

      try {
        unowned Thread <void *> thread = Thread.create <void *> (syspeek.run, true);
      } catch (ThreadError e) {
        stderr.printf ("main: %s\n", e.message);
        return 1;
      }
    #else
      var time = new TimeoutSource (1000);

      time.set_callback (syspeek.run);
      time.set_priority (Priority.DEFAULT_IDLE);
      time.attach       (null);
    #endif

    Gtk.main ();
    return 0;
  }
}

//  -------------------------------------------------------------------------------------

#if DEBUG
  public class SystemIndicator {
    private const string ICON      = "syspeek";
    private const string [] FORMAT = { "CPU: %.1f%%",
                                       "------------------",
                                       "Memory: %s of %s",
                                       "Swap: %s of %s",
                                       "------------------",
                                       "Disk: %s of %s",
                                       "------------------",
                                       "Receiving: %s/s (%s)",
                                       "Sending: %s/s (%s)",
                                       "------------------" };

    public void update (int index, float value, ...) {
      var args = va_list ();
      var i = ((int) (value * 10.0f)) * 10;
      var s = "%s-%d".printf (ICON, i);
      stdout.vprintf (FORMAT [index] + " (" + s + ")\n", args);
    }
  }
#endif
