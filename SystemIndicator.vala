using Gtk;
using AppIndicator;

public class SystemIndicator : Window {
  private const string NAME      = "SystemIndicator";
  private const string ICON      = "syspeek";
  private const string VERSION   = "0.3";
  private const string COMMENT   = "Now stays even crunchier in milk\nwith the power of Vala";
  private const string LICENSE   = "GPL-3";
  private const string COPYRIGHT = "Copyright (c) 2011 Georg Schmidl";
  private const string WEBSITE   = "http://launchpad.net/syspeek";

  private const string    LABEL  =   "%-40s";
  private const string [] FORMAT = { "CPU: %.1f%%",
                                     "--------------------",
                                     "Memory: %s of %s",
                                     "Swap: %s of %s",
                                     "--------------------",
                                     "Disk: %s of %s",
                                     "--------------------",
                                     "Receiving: %s/s (%s)",
                                     "Sending: %s/s (%s)",
                                     "--------------------" };

  private const string SYSMON_COMMAND = "gnome-system-monitor";
  private const string SYSMON_ICON    = "utilities-system-monitor.svg";
  public  Indicator    appIcon;

  private Gtk.Menu             trayMenu;
  private Gtk.RadioMenuItem [] radioItem;
  private Gtk.Label []         radioLabel;
  private string []            radioText;
  private string               icons [11];
  private int                  selected = 0;
  private int                  lastIcon = 0;
  private bool                _visible  = true;

  public SystemIndicator () {
    unowned SList<Gtk.RadioMenuItem> radioGroup = null;
    trayMenu     = new Gtk.Menu ();
    var menuItem = new Gtk.MenuItem ();

    radioItem.resize  (FORMAT.length);
    radioLabel.resize (FORMAT.length);
    radioText.resize  (FORMAT.length);

    int i = 0;
    foreach (string s in FORMAT) {
      if (s [0] == '-') {
        radioItem [i++] = null;
//      menuItem = new Gtk.MenuItem ();
        menuItem = new Gtk.SeparatorMenuItem ();
        trayMenu.append (menuItem);
      } else {
        radioText  [i] = LABEL.printf (s);
        radioItem  [i] = new Gtk.RadioMenuItem.with_label (radioGroup, radioText [i]);
        radioLabel [i] = (Label) radioItem [i].get_child ();
        radioItem  [i].activate.connect (select);
        radioGroup = radioItem [i].get_group ();
        trayMenu.append (radioItem [i++]);
      }
    }

    radioItem [0].set_active (true);

    var image     = new Image.from_file (SYSMON_ICON);
    var imageItem = new Gtk.ImageMenuItem ();
    imageItem.set_label ("System _Monitor\t\t\t\t\t");
    imageItem.set_image (image);
    imageItem.activate.connect (() => {
      try {
        Process.spawn_command_line_async (SYSMON_COMMAND);
      } catch (Error e) { error ("SystemMonitor: %s", e.message); }
    });
    imageItem.always_show_image = true;
    imageItem.use_underline     = true;
    trayMenu.append (imageItem);

    imageItem = new Gtk.ImageMenuItem.from_stock (Stock.ABOUT, null);
    imageItem.activate.connect (about_dialog);
    imageItem.always_show_image = true;
    trayMenu.append (imageItem);

    imageItem = new Gtk.ImageMenuItem.from_stock (Stock.QUIT, null);
    imageItem.activate.connect (Gtk.main_quit);
    imageItem.always_show_image = true;
    trayMenu.append (imageItem);

    trayMenu.double_buffered = true;
    trayMenu.show_all ();
/*
    trayMenu.notify.connect ((s, p) => {
      print ("trayMenu:   Property '%s' has changed!\n", p.name);
    });
*/
    for (i = 0; i <= 10; i++) icons [i] = ICON + "-" + (10*i).to_string ();

    appIcon = new Indicator  (NAME, ICON, IndicatorCategory.APPLICATION_STATUS);
    appIcon.set_status       (IndicatorStatus.ACTIVE);
    appIcon.set_icon         (@"$ICON-0");
    appIcon.set_menu         (trayMenu);
  }

//  -------------------------------------------------------------------------------------
//  Make results more readable by appending the appropriate metric unit to the data
//  -------------------------------------------------------------------------------------

  private const string suffix [] = { "bytes", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB" };

  private string readable (float bytes) {
    foreach (string s in suffix) {
      if (bytes < 1024.0) {
        var format = (s == "bytes") ? "%.0f %s" : "%.1f %s";
        return format.printf (bytes, s);
      }
      bytes = bytes / 1024.0f;
    }
    return "%.1f YiB".printf (bytes);
  }

//  -------------------------------------------------------------------------------------
//  Menu update
//  -------------------------------------------------------------------------------------

  public void update (int item, float pct, float val1, float? val2 = 0.0f) {
//  if (_visible) {
      weak string text = (item == 0) ? FORMAT [item].printf (val1)
                                     : FORMAT [item].printf (readable (val1), readable (val2));
      if (text != radioLabel [item].get_text()) {
        radioLabel [item].set_text (LABEL.printf (text));
      }
//  }
    var ptr = (int) (pct * 10.0f);

    if (ptr != lastIcon) {
      lastIcon = ptr;
      if (selected == item) appIcon.set_icon      (icons [ptr]);
    }
  }

  public void checkVisibility () {
    var viz = trayMenu.visible;
    if (viz != _visible) _visible = viz;
  }

//  -------------------------------------------------------------------------------------
//  Radio button selection
//  -------------------------------------------------------------------------------------

  private void select () {
    for (int i = 0; i < radioItem.length; i++) {
      if (radioItem [i] != null && radioItem [i].active) selected = i;
    }
//  print (@"selected = $selected\n");
  }

//  -------------------------------------------------------------------------------------
//  About dialog
//  -------------------------------------------------------------------------------------

  private void about_dialog () {
    var about = new AboutDialog ();

    about.set_program_name   (NAME);
    about.set_version        ("\nv" + VERSION);
    about.set_comments       (COMMENT);
    about.set_license        (LICENSE);
    about.set_copyright      (COPYRIGHT);
    about.set_website        (WEBSITE);
    about.set_logo_icon_name (ICON);
    about.run     ();
    about.destroy ();
  }
}

//  -------------------------------------------------------------------------------------

#if DEBUG
  public static int main (string[] args) {
    Gtk.init (ref args);

    var indicator = new SystemIndicator ();
    indicator.hide ();

    Gtk.main ();
    return 0;
  }
#endif
