PKG = --pkg=glib-2.0 --pkg=gtk+-3.0 --pkg=appindicator3-0.1 --pkg=gio-2.0

SysPeek: SysPeek.vala SystemIndicator.vala
	valac $(PKG) -g SystemIndicator.vala SysPeek.vala -o SysPeek

DLMALLOC:
	valac -D DLMALLOC $(PKG) -g dlmalloc.c set_vtable.c SystemIndicator.vala SysPeek.vala -o SysPeek

profile: SysPeek.vala SystemIndicator.vala
	valac -D PROFILE $(PKG) --enable-mem-profiler -g SystemIndicator.vala SysPeek.vala -o SysPeek

debug:
	valac -D DEBUG $(PKG) SystemIndicator.vala
	valac -D DEBUG $(PKG) SysPeek.vala
