This is a port of SysPeek to Vala which is originally written in Python.  
Original SysPeek project can be found at https://launchpad.net/syspeek  
 
 
The entire porting is done by Bruce Reidenbach (https://launchpad.net/~bereiden)  
This repo only serves as backup for his SysPeek-Vala port.
